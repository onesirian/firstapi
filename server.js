var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var ingredients = [
    {
        "id": "232kak",
        "text": "eggs"
    },
    {
        "id": "232kal",
        "text": "milk"
    },
    {
        "id": "232kam",
        "text": "bread"
    }
];
app.get('/',function(request, response) {
    response.send(ingredients);
});
app.get('/funions',function(request, response) {
    response.send('You Found your FUNIONS!');
});
app.listen(3000, function () {
    console.log("First API running on port 3000!");
});
